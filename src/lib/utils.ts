export function formatDate(date: Date) {
	return new Intl.DateTimeFormat('en', { dateStyle: 'long' }).format(date)
}

export async function sleep(ms: number) {
	return new Promise<void>((resolve) => setTimeout(resolve, ms))
}
