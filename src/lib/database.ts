import { PrismaClient } from '@prisma/client'

const db = new PrismaClient()

export default db

type Todo = {
	id: number
	text: string
	completed: boolean
}

let todos: Todo[] = [
	{
		id: Date.now(),
		text: 'Learn about forms',
		completed: false
	}
]

export function getTodos() {
	return todos
}

export function addTodo(text: string) {
	const todo: Todo = {
		id: Date.now(),
		text,
		completed: false
	}
	todos.push(todo)
}

export function removeTodo(id: number) {
	todos = todos.filter((todo) => todo.id !== id)
}

export function clearTodo() {
	todos = []
}
