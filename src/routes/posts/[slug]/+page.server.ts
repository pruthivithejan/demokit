import db from '$lib/database'
import { error } from '@sveltejs/kit'
import type { PageServerLoad } from './$types'

export const load: PageServerLoad = async ({ params }) => {
	const post = await db.post.findUnique({
		where: { slug: params.slug }
	})

	if (!post) {
		throw error(404, 'Post not found')
	}

	return { post }
}

// Dealing with .env or fetching from a database you have to use a +page.server.ts file.
