import type { RequestHandler } from '@sveltejs/kit'

// POST request
export const POST: RequestHandler = async (event) => {
	const data = await event.request.formData()
	const email = data.get('email')

	console.log(email)

	return new Response(JSON.stringify({ success: true }), {
		headers: { 'Content-Type': 'application/json' }
	})
}
